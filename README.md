# splyt-node-app
##### Project basics
- Clone project using https://gitlab.com/jetjet.dev/splyt-node-app.git 
- Checkout `main` branch

##### Steps to run and debug the application on http://localhost:3000/ using Visual Studio Code
1. Run `npm i` to install dependencies and required files in the terminal
2. Go to `Run and Debug` tab
3. Select `Debug` from the dropdown
4. Hit `Start Debugging` button (green play button)
5. Navigate to http://localhost:3000/help in browser

##### Building the project and run using command line
1. Run `npm run build` to build the project 
2. The build artifacts will be available in the `dist/` directory
3. Run `npm run start` it will serve http://localhost:3000/ using the built files
