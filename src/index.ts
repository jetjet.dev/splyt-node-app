import * as express from 'express';
import * as http from 'http';
import * as https from 'https';
import * as cors from 'cors';
import * as compression from 'compression';
import * as helmet from 'helmet';
import * as config from 'config';
import logger from './lib/logger';
import * as expressWinston from 'express-winston';
import winston = require('winston');
import routes from './routers';
import * as openApiValidator from 'express-openapi-validator';
import * as swagger from 'swagger-ui-express';
import path = require('path');
import * as yamljs from 'yamljs';

const configure = async () => {
  const app = express();
  // Standard configuration
  app.use(cors());
  app.use(compression());
  app.use(express.json());
  app.use(helmet());

  // Configure request logger
  app.use(expressWinston.logger({
    transports: [
      new winston.transports.Console()
    ],
    format: winston.format.combine(
      winston.format.json(),
      winston.format.timestamp()
    ),
    meta: false,
    expressFormat: true,
    ignoreRoute: (req, res) => {
      return req.path.indexOf('/help') >= 0
    }
  }));

  // Swagger UI & Open API validation
  const apiSpec = path.join(__dirname, '../openapi.yaml');

  app.use('/help', swagger.serve, swagger.setup(yamljs.load(apiSpec)));
  app.use(
    openApiValidator.middleware({ apiSpec })
  );

  // Routes
  app.use('/api', routes);

  app.use((err, req, res, next) => {
    logger.error(err);
    // Validate responses error by OpenApiValidator
    if (err.status === 400 && err.message && err.errors) {
      res.status(err.status).json({
        message: err.message,
        err: err.errors,
      });
    } else if (err.status === 404) {
      res.status(404).json({ err: 'Resource not found.' });
    } else {
      res.status(500).json({ err: 'Internal server error.' });
    }
  });

  await new Promise<void>(async (resolve, reject) => {
    const protocolConfig = config.get('server.protocol') || 'http';
    const port = config.get('server.port') || process.env.PORT;
    const server = (String(protocolConfig).toLowerCase() === 'https') ? https.createServer(app) : http.createServer(app);

    server.listen(port, function () {
      logger.info(`${config.get('app.name')} is now listening to port ${port}`);
      resolve();
    });
    server.on('error', (err) => {
      logger.error(err)
      reject();
    })
  });
};
(async () => {
  await configure();
})();