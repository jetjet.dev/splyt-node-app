import axios from 'axios';
import * as config from 'config';
import logger from '../lib/logger';
import { GetTaxiDriverParams, TaxiDriverList } from '../models/taxi';

export async function getTaxiDriver(params: GetTaxiDriverParams): Promise<TaxiDriverList> {
  let url = `${config.get('api.host')}/drivers?`;
  url += `latitude=${params.latitude}&longitude=${params.longitude}&count=${params.count}`;

  try {
    const response = await axios.request<TaxiDriverList>({
      method: 'get',
      url
    })
    return response.data;
  } catch (err) {
    logger.error(err);
    return null;
  }
}

