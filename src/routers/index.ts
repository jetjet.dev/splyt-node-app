import taxiRouter from './taxi';
import { Router } from 'express';
const router = Router();

router.use('/taxi', taxiRouter);

export default router;
