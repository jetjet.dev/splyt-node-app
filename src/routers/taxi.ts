import * as express from 'express';
import { getTaxiDriver } from '../services/taxiService';
const router = express.Router();

router.get('/drivers/:latitude/:longitude/:count', async (req, res, next) => {
  try {
    const result = await getTaxiDriver({ 
      latitude: Number(req.params.latitude),
      longitude: Number(req.params.longitude), 
      count: Number(req.params.count)
    });
    res.json(result);
  } catch (err) {
    next(err);
  }
});

export default router;