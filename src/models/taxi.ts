// API response interface
export interface TaxiDriverList {
  pickup_eta: number;
  drivers: Driver[];
}

export interface Driver {
  driver_id: string;
  location: DriverLocation;
}

export interface DriverLocation {
  latitude: number;
  longitude: number;
  bearing: number;
}

// API parameter interface
export interface GetTaxiDriverParams {
  latitude: number;
  longitude: number;
  count: number;
}