import * as winston from 'winston';
import * as config from 'config';

const logger = winston.createLogger({
  level: config.get('log.level'),
  format: winston.format.combine(
    winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss:ms' }),
    winston.format.errors({
      stack: true
    }),
    winston.format.printf(
      (info) => `${info.timestamp} ${info.level}: ${info.message}`,
    ),
    winston.format.json(),
  ),
  transports: [
    new winston.transports.Console()
  ],
});

export default logger;